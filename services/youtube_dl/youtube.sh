#!/usr/bin/env bash
echo ${@}
( export DISPLAY=:0; omxplayer --win "0 0 800 480" --vol -3600 $(youtube-dl -g -f best https://www.youtube.com/watch?v=${@}) & )
