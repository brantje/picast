var cp = require('child_process');
var youtubedl = require('youtube-dl');

function play(params, cb) {
    var url = params.url;
    console.log('getting vid info')
    // Optional arguments passed to youtube-dl.
    var options = ['-f best'];
    youtubedl.getInfo(url, options, function (err, info) {
        if (err) {
            cb(false);
        }
        if (!info) {
            cb(false);
        }
        var _h, _m, _s;
        var duration_re = /(\d+)?:?(\d+)?:(\d+)/g;
        var e = duration_re.exec(info.duration);
        _h = e[1];
        _m = e[2];
        _s = e[3];

        if (e[2] === undefined) {
            _h = 0;
            _m = e[1];
        }

        var h = parseInt(_h * 60 * 60);
        var m = parseInt(_m * 60);
        var s = parseInt(_s);
        var file = {
            id: info.id,
            title: info.title,
            url: info.url,
            thumbnail: info.thumbnail,
            duration: (h + m + s),
            format: info.format
        };
        info.description = '';
        cb(file)
    });
}

function get_extractors(cb) {
    cp.exec('youtube-dl --list-extractors', function (error, stdout, stderr) {
        cb(stdout.toString().trim().split("\n"));
    });
}

module.exports = function (module_holder) {
    // the key in this dictionary can be whatever you want
    // just make sure it won't override other modules
    module_holder['youtube_dl'] = {
        play: play,
        get_extractors: get_extractors
    };
};
