'use strict';

/**
 * @ngdoc function
 * @name picastApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the picastApp
 */
angular.module('picastApp')
    .controller('HomeCtrl', ['$scope', 'SocketFactory', function ($scope, SocketFactory) {

        $scope.playUrl = function (url) {
            $scope.log = [];
            SocketFactory.emit('playUrl', {url: url, service: 'youtube_dl'});
        };

        SocketFactory.on('message', function (data) {
            console.log(data)
        });
    }]);
