'use strict';

/**
 * @ngdoc function
 * @name picastApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the picastApp
 */
angular.module('picastApp')
    .controller('MainCtrl', ['$scope', 'SocketFactory', function ($scope, SocketFactory) {
        this.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        $scope.playerAction = function (action) {
            SocketFactory.emit('player_action', {action: action});
        };

        SocketFactory.on('player_status_update', function (data) {
            $scope.status = data;
        });
        $scope.log = [];
        SocketFactory.on('player_stdout', function (data) {
            $scope.log.push(data);
        });
    }]);
