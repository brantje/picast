'use strict';

/**
 * @ngdoc service
 * @name picastApp.SocketService
 * @description
 * # SocketService
 * Service in the picastApp.
 */
angular.module('picastApp')
  .service('SocketService', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
