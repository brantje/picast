'use strict';

describe('Filter: secondsToTime', function () {

  // load the filter's module
  beforeEach(module('picastApp'));

  // initialize a new instance of the filter before each test
  var secondsToTime;
  beforeEach(inject(function ($filter) {
    secondsToTime = $filter('secondsToTime');
  }));

  it('should return the input prefixed with "secondsToTime filter:"', function () {
    var text = 'angularjs';
    expect(secondsToTime(text)).toBe('secondsToTime filter: ' + text);
  });

});
