module.exports = function (grunt) {
    var jsResources = [];
    // Project configuration.
    grunt.initConfig({
        jsResources: [],
        cssResources: [],
        pkg: grunt.file.readJSON('package.json'),
        html2js: {
            options: {
                // custom options, see below
                base: 'templates',
                quoteChar: '\'',
                useStrict: true,
                htmlmin: {
                    collapseBooleanAttributes: false,
                    collapseWhitespace: true,
                    removeAttributeQuotes: false,
                    removeComments: true,
                    removeEmptyAttributes: false,
                    removeRedundantAttributes: false,
                    removeScriptTypeAttributes: false,
                    removeStyleLinkTypeAttributes: false
                }
            },
            main: {
                src: ['templates/views/**/*.html'],
                dest: 'js/templates.js'
            }
        },
        jshint: {
            options: {
                reporter: require('jshint-stylish'),
                curly: false,
                eqeqeq: true,
                eqnull: true,
                browser: true,
                globals: {
                    "angular": true,
                    "PassmanImporter": true,
                    "PassmanExporter": true,
                    "OC": true,
                    "window": true,
                    "console": true,
                    "CRYPTO": true,
                    "C_Promise": true,
                    "forge": true,
                    "sjcl": true,
                    "jQuery": true,
                    "$": true,
                    "_": true,
                    "oc_requesttoken": true
                }
            },
            all: ['js/app/**/*.js']
        },
        sass: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: "sass",
                        src: ["app/styles/**/app.scss"],
                        dest: "css",
                        ext: ".css"
                    },
                    {
                        expand: true,
                        cwd: "sass",
                        src: ["**/bookmarklet.scss"],
                        dest: "css",
                        ext: ".css"
                    },
                    {
                        expand: true,
                        cwd: "sass",
                        src: ["**/public-page.scss"],
                        dest: "css",
                        ext: ".css"
                    },
                    {
                        expand: true,
                        cwd: "sass",
                        src: ["**/admin.scss"],
                        dest: "css",
                        ext: ".css"
                    }
                ]
            }
        },

        karma: {
            unit: {
                configFile: './karma.conf.js',
                background: false
            }
        },

        //@TODO JSHint
        watch: {
            scripts: {
                files: ['Gruntfile.js', 'templates/views/{,*/}{,*/}{,*/}*.html', 'templates/views/*.html', 'sass/*', 'sass/partials/*'],
                tasks: ['html2js', 'sass'],
                options: {
                    spawn: false,
                    interrupt: true,
                    reload: true
                }
            }
        },
        /**
         * Build commands
         */
        mkdir: {
            dist: {
                options: {
                    mode: 0700,
                    create: ['dist']
                }
            }
        }



    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-mkdir');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-replace');


    // Default task(s).
    grunt.registerTask('default', ['html2js', 'sass']);
    grunt.registerTask('hint', ['jshint']);
    grunt.registerTask('build', ['sass', 'jshint', 'html2js', 'mkdir:dist', 'copy:dist', 'copy:fonts', 'replace:dist', 'uglify', 'concat:css', 'cssmin', 'clean:css', 'replace:strict', 'copy:settingsJs']);

};