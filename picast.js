var exec = require('child_process').exec;
var express = require('express');
var path_module = require('path');
var fs = require('fs');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var env = process.env;
var service_holder = {};
var player = require('./lib/omx/player');
var pngview = require('./lib/pngview/pngview');


function LoadModules(path) {
    fs.lstat(path, function (err, stat) {
        if (stat.isDirectory()) {
            // we have a directory: do a tree walk
            fs.readdir(path, function (err, files) {
                var f, l = files.length;
                for (var i = 0; i < l; i++) {
                    f = path_module.join(path, files[i]);
                    LoadModules(f);
                }
            });
        } else {
            if (path.indexOf('.js') !== -1) {
                // we have a file: load it
                console.log('Loading service: ' + path)
                require(path)(service_holder);
            }
        }
    });
}

var DIR = path_module.join(__dirname, 'services');
LoadModules(DIR);

io.on('connection', function (socket) {
    console.log('Client connected!')
    socket.on('playUrl', function (data) {
        player.stop();
        pngview.show()

        if (data.url) {
            service_holder[data.service].play(data, function(searchResult){
                player.play(searchResult);
            })
        }
    });

    setInterval(function () {
        socket.emit('player_status_update', player.getStatus());
        if(player.isPlaying()){
            pngview.hide()
        }

    }, 1000);

    socket.on('player_action', function (data) {
        player[data.action]()
    });


});

player.on('stdout', function (data) {
    io.sockets.emit('player_stdout', data)
});

player.on('error', function (data) {
    io.sockets.emit('player_error', data)
});

player.on('forwards', function (data) {
    io.sockets.emit('player_status_update', player.getStatus());
});

player.on('backwards', function (data) {
    io.sockets.emit('player_status_update', player.getStatus());
});

player.on('pause', function (data) {
    io.sockets.emit('player_status_update', player.getStatus());
});

player.on('play', function (data) {
    io.sockets.emit('player_status_update', player.getStatus());
});

player.on('quit', function (data) {
    io.sockets.emit('player_status_update', player.getStatus());
});


player.on('stop', function (data) {
    io.sockets.emit('player_status_update', player.getStatus());
});

player.on('next', function (data) {
    io.sockets.emit('player_status_update', player.getStatus());
});

app.use(express.static(path_module.join(__dirname, 'public/app')));

app.post('/:service', function (req, res) {
    res.send('Streaming video...');
    player.stop();
    pngview.show()
    service_holder[req.params.service].play({url: req.body.url}, player);
});

app.get('/stop', function (req, res) {
    res.send('Stopping Stream...');
    exec("./stop-stream.sh", {env: env},
        function (error, stdout, stderr) {
            console.log(error, stdout, stderr)
        });
});

app.get('/extractors', function (req, res) {
    //youtube-dl --list-extractors
    service_holder['youtube_dl'].get_extractors(function(extractors){
        res.json(extractors)
    });

});

// Setup PiCAST Server
server.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Access at http://%s:%s', host, port);
});
