var EventEmitter = require('events').EventEmitter;
var exec = require('child_process').exec;
var fs = require('fs');

var pngview = function () {
    var that = {};

    var show = function () {
        var path = __dirname;
        var pngview = path +'/bin/pngview';
        var image = path+ '/img/' + 'loading.png';
        var cmd = pngview;
        var args = ['-d', '0', '-l', '2', image];
        var command = cmd + ' '+ args.join(' ');
        exec(command)
    };

    var hide = function () {
        exec("kill $(ps aux | grep pngview | grep loading.png | awk '{print $2}')");
    };

    that.show = show;
    that.hide = hide;
    return that
};

module.exports = pngview();