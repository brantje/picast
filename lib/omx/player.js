var spawn = require('child_process').spawn;
var util = require('util');
var path = require('path');
var fs = require('fs');
var EventEmitter = require('events').EventEmitter;
var LoopHelper = require('./loop_helper.js').LoopHelper;
var exec = require('child_process').exec;
var onlyOneProcess = require('./watchdog.js').onlyOneProcess;

/* keep compatibility with older versions of nodejs */
if (fs.existsSync) {
    path.existsSync = fs.existsSync;
} else {
    fs.existsSync = path.existsSync;
}

var player = function () {

    var that = Object.create(EventEmitter.prototype);

    var nativeLoop = false;
    var handleExitHang = false;

    var videoDir = './';
    var videoSuffix = '';

    var currentVideos = [];
    var currentSettings = {};
    var currently_playing = {
        progress: 0,
        audio: {},
        video: {}
    };
    var loopHelper = null;

    var commands = {
        'pause': 'p',
        'quit': 'q',
        'volup': '+',
        'voldown': '-',
        'backwards':'\x1b\x5b\x44',
        'forwards':'\x1b\x5b\x43',
    };

    var omxProcess = null;
    var paused = false;

    var resetCurrentlyPlaying = function () {
        currently_playing = {
            progress: 0,
            audio: {},
            video: {}
        };
    };

    var sendAction = function (action) {
        if (commands[action] && omxProcess) {
            try {
                omxProcess.stdin.write(commands[action], function (err) {
                    //console.log(err);
                });
            } catch (err) {
                //console.log(err);
            }
        }
    };

    var resolveFilePaths = function (videos) {
        /* reset currentFiles, it will contain only valid videos */
        currentVideos = [];
        var ret = [];
        var re = /(rtsp|rtmp|udp|http|https)/;
        videos.forEach(function (video) {
            var uri = video.url;
            if(re.exec(uri) != null) {
                ret.push(video);
                currentVideos.push(video);

                return;
            }
            else
            {
                var realPath = path.resolve(videoDir, uri + videoSuffix);
                if (fs.existsSync(realPath)) {
                    ret.push(realPath);
                    currentVideos.push(video);
                } else {
                    that.emit('error', new Error('File does not exist: ' + realPath));
                }
            }
        });
        return ret;
    };


    /*
     * Get video length and check, after its duration, if
     * process is hanging. Correct a bug of omxplayer
     * https://github.com/popcornmix/omxplayer/issues/124
     * https://github.com/popcornmix/omxplayer/issues/12
     */
    var startWatchdog = function (filename, pid) {
        if (!handleExitHang) {
            return;
        }
        getVideoLength(filename, function (durationMs) {
            setTimeout(function () {
                if (pid === omxProcess.pid) {
                    omxProcess.kill('SIGKILL');
                }
            }, durationMs + 10000);
        });
    };

    var parseStdout = function (data) {
        var v,a,p;
        var progress_re = /M:(\d+)/g;
        var video_re = /Video codec\s([a-z0-9-]+)\swidth\s([0-9]+)\sheight\s([0-9]+)\sprofile\s([0-9]+)\sfps\s([0-9.]+)/g;
        var audio_re = /Audio codec\s([a-z0-9]+)\schannels\s([0-9]+)\ssamplerate\s([0-9]+)\sbitspersample\s([0-9]+)/g;
        if(v = video_re.exec(data)){
            currently_playing.video = {
                codec: v[1],
                width: parseInt(v[2]),
                height: parseInt(v[3]),
                profile: v[4],
                fps: parseInt(v[5])
            }
        }
        if(a = audio_re.exec(data)){
            currently_playing.audio = {
                codec: a[1],
                channels: parseInt(a[2]),
                sample_rate: parseInt(a[3]),
                bit_rate: parseInt(a[4])
            }
        }
        if(p = progress_re.exec(data)){
            currently_playing.progress = Math.ceil(p[1] / 1000 / 1000);
        }
    };

    var open = function (videos, options) {
        exec("export DISPLAY=:0; xdpyinfo | grep dimensions", function (error, stdout, stderr) {
            var regex = /[^0-9]*([0-9]+x[0-9]+) pixels/g;
            var m = regex.exec(stdout)[1].split('x');
            var w = m[0];
            var h = m[1];
            var settings = options || {};
            currentSettings = settings;
            var cmd = 'omxplayer';
            var respawn = null;
            var args = ['--win','0,0,'+ w +','+ h, '-s', '--layer','5'];
            if (settings.audioOutput && settings.audioOutput !== 'default') {
                args.push('-o');
                args.push(settings.audioOutput);
            }

            if (settings.loop === true) {
                if (nativeLoop) {
                    args.push('--loop');
                }
            }

            if (settings.osd == false){
                args.push('--no-osd')
            }

            if (settings.args) {
                args = args.concat(settings.args);
            }

            if (typeof videos === 'object') {
                videos = [videos];
            }



            var realfiles = resolveFilePaths(videos);

            if (nativeLoop) {
                /* all files to omxplayer parameters */

                for(var i = 0; i < videos.length; i++){
                    // Not implemented yet @todo
                }
                args.push.apply(args, realfiles);
            } else {
                /*
                 * only first file to omxplayer parameter. following files are handled by
                 * loopHelper
                 */
                args.push(realfiles[0].url);
            }

            if (!nativeLoop && ((realfiles.length > 1) || (settings.loop))) {
                /* no native loop support, enable helper */
                loopHelper = LoopHelper(realfiles, settings.loop);

                respawn = function () {
                    if (!loopHelper) {
                        /* respawn ignored, stop requested */
                        resetCurrentlyPlaying();
                        that.emit('stop');
                        return;
                    }
                    /* change file */
                    var nextFile = loopHelper.getNext();
                    if (!nextFile) {
                        /* respawn ignored, loop ended */
                        loopHelper = null;
                        resetCurrentlyPlaying();
                        that.emit('stop');
                    } else {
                        /* Issue #16: Add event emitter when next file plays */
                        that.emit('next', nextFile);
                        /* respawn */
                        args[args.length - 1] = nextFile;
                        omxProcess = spawn(cmd, args, {
                            stdio: ['pipe', null, null]
                        });
                        omxProcess.once('exit', respawn);
                        /* check if omxplayer hangs when video should be finished */
                        startWatchdog(nextFile, omxProcess.pid);
                    }
                };
                omxProcess = spawn(cmd, args);
                omxProcess.stdout.on('data', function (data) {
                    //that.emit('stdout', data.toString());
                    parseStdout(data.toString());
                });
                omxProcess.once('exit', respawn);
            } else {
                /* native loop support enabled or not requested */
                currently_playing = Object.assign(currently_playing, videos[0]);
                omxProcess = spawn(cmd, args);
                omxProcess.stdout.on('data', function (data) {
                    //that.emit('stdout', data.toString());
                    try{
                        parseStdout(data.toString());
                    } catch (e) {
                        that.emit('error', data.toString());
                    }
                });
                omxProcess.once('exit', function (code, signal) {
                    omxProcess = null;
                    resetCurrentlyPlaying();
                    that.emit('stop');
                });
            }

            that.emit('load', videos, options);
        });
    };

    var play = function (videos, options) {
        if (omxProcess) {
            if (!paused) {
                return false;
            }
            sendAction('pause');
            paused = false;
            that.emit('play');
            return true;
        }
        if (!videos) {
            throw new TypeError("No files specified");
        }
        if (typeof videos !== 'object' && !util.isArray(videos)) {
            throw new TypeError("Incorrect value for videos: " + videos);
        }
        open(videos, options);
        that.emit('play');
        return true;
    };

    var pause = function () {
        if (paused) {
            return false;
        }
        sendAction('pause');
        paused = true;
        that.emit('pause');
        return true;
    };

    var volup = function () {
        sendAction('volup');
        that.emit('volup');
        return true;
    };

    var voldown = function () {
        sendAction('voldown');
        that.emit('voldown');
        return true;
    };

    var backwards = function () {
        sendAction('backwards');
        that.emit('backwards');
        return true;
    };

    var forwards = function () {
        sendAction('forwards');
        that.emit('forwards');
        return true;
    };

    var handleQuitTimeout = function (oldOmxProcess, timeout) {
        var timeoutHandle = setTimeout(function () {
            console.log('omxplayer still running. kill forced');
            oldOmxProcess.kill('SIGTERM');
        }, timeout);
        oldOmxProcess.once('exit', function () {
            clearTimeout(timeoutHandle);
        });
    };

    var stop = function () {
        if (!omxProcess) {
            /* ignore, no omxProcess to stop */
            return false;
        }
        paused = false;
        that.emit('quit');
        currently_playing = {};
        loopHelper = null;
        sendAction('quit');
        handleQuitTimeout(omxProcess, 250);
        omxProcess = null;
        return true;
    };

    var isPlaying = function () {
        return omxProcess && !paused;
    };

    var isLoaded = function () {
        return omxProcess;
    };

    var getStatus = function () {
        if (isLoaded()) {
            return {
                videos: currentVideos,
                settings: currentSettings,
                playing: isPlaying(),
                currently_playing: currently_playing,
                loaded: true
            };
        }
        return {
            loaded: false
        };
    };

    var setVideoDir = function (dir) {
        videoDir = dir;
    };

    var setVideoSuffix = function (suffix) {
        videoSuffix = suffix;
    };

    var enableNativeLoop = function () {
        nativeLoop = true;
        return that;
    };

    setInterval(onlyOneProcess, 5000);

    that.play = play;
    that.pause = pause;
    that.stop = stop;
    that.backwards = backwards;
    that.forwards = forwards;
    that.volup = volup;
    that.voldown = voldown;
    that.isPlaying = isPlaying;
    that.isLoaded = isLoaded;
    that.getStatus = getStatus;
    that.setVideoDir = setVideoDir;
    that.setVideoSuffix = setVideoSuffix;
    that.enableNativeLoop = enableNativeLoop;

    return that;
};

module.exports = player();